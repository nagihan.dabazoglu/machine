# 2. CAD Design

This week I worked on defining my final project had an overview for the CAD program "Autodesk Fusion 360".
I decided to make a Lasercutter for my final project so I want to use the 2D and 3D Model sketches for my final project

## Useful links

- [Autodesk Fusion 360](https://www.autodesk.de/products/fusion-360/students-teachers-educators)
- [Grabcad](https://grabcad.com)


### CAD's which I used
![](../images/week02/alu.png)
[Download Link](https://grabcad.com/library/perfil-de-30x30-1)

### How to import a CAD - Model
A lot of CAD-Models are available on Grabcad so you can download the files on this site.
After you downloaded your file you have to import in Fusion 360. On the left side of your projects bar you see your your projects and in your project folder you can upload files.
![](../images/week02/projectfolder.png)


## Frame of  my lasercutter
![](../images/week02/lasercutterframe.png)

## How to build this

#### Specifications
##### Size of the casing: 727 x 538 x 175 mm
##### size of  the working surface: 500 x 390 mm

Create Sketch:
First of all i created sketches in the real sizes:
![](../images/week02/week2_sketch1.png)

Extrude: Then I extruded the sketches
![](../images/week02/week2_sketch2.png)

Place it on the right place:
![](../images/week02/week2_sketch3.png)

Created the working space:
![](../images/week02/week2_sketch4.png)

Created the slider
![](../images/week02/week2_sketch5.png)

Duplicated it and place it on the right Place
![](../images/week02/week2_sketch6.png)

Create a bolt
1. Create a sketch
![](../images/week02/week2_sketch7.png)
2. Extrude this sketch
![](../images/week02/week2_sketch8.png)
3. Create a circular pattern
![](../images/week02/week2_sketch9.png)

This bolt is for the grid but in this model I haven't built the grid.

Create a box for moving on the y-axis an place for the cables
![](../images/week02/week2_sketch10.png)

Import a alu-grid for the laser
![](../images/week02/week2_sketch11.png)

Cover which can be opened and closed
![](../images/week02/week2_sketch12.png)

## Download
[Download](../files/LaserCutterv2.f3d)
