# 1. Principles and practices

This week we had an introduction to the the documentation process.

## Research
### Tools for publishing the documentation:
#### Markdown
#### Git

## Useful links

- [Markdown](https://en.wikipedia.org/wiki/Markdown)

## Markdown

Markdown

Markdown is a way to write content for the web. It'written in "plaintext" and is just the regular alphabet.

The common commands:

```
// Differet Header sizes with the hastag symbol '#'

# H1
## H2
### H3
#### H4
##### H5
###### H6

// write in italics
*italics* or _italics_

// write in bold
**bold** or __bold__

//write in bold & italics
**_bold & italic_**

// Links
write the link text in brackest []
and the link in paranthesis ()
[your text here](www.yourlink.com)

//Images
the same way of link just with an exclamation point
![name of image](link)

// Blockquotes
to create a blockquote you have to use the " greater than" caret '>'

//Lists
for an unordered list you have to use an asteri '*'
for an ordered list you have to use numbers
'1'
'2'
'3'
'..'

```

## Git

For publishing and organising the files we use Git. First of all you have to create a GitLab account. [On this site](www.gitlab.com)

For publishing the docmetation I used the template. Which you can download from [here](https://drive.google.com/open?id=1aYQPWW6qPtWto6JaSDFNvUzz39zGkopO).

###Important git commands

####add all files in the folder:
```
git add .
```

####commit changes:
```
git commit -, "commit message"
```
####push the files:
```
git push
```

## Problems to upload my files
I had problems to upload my changes & files because my email adress and my name were configured automatically and i had to change this with this commands:

```
 git config --global user.name "Your Name"
 git config --global user.email you@example.com
```
