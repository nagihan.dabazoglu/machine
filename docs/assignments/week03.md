# 3. Laser Cutting

This week we worked with the laser cutter and learned how the laser cutter works and how to produce machine parts with the laser cutter. For this weeks assignment we have to produce a machine part on which we can stand.

![](../images/week03/week3_parts.jpg)

## Settings for the laser cutter
```
// 9mm Acrylic
Speed: 3 %
Freq: 100 %
Power: 100 %

// 12mm POM (to cut this we have to use this settings 2x)
Speed: 3 %
Freq: 100 %
Power: 100 %
(to cut this we have to use this settings 2x)
```



## My machine part:
![](../images/week03/week3_mypart.JPG)
My construction with the 9mm Acrylic is not a final part which I can use for my own laser cutter but this structure could be used for to place the laser inside.

## How I produced this part
![](../images/week03/week3_sketch.png)
I created a sketch in Fusion360 and export this file to [Rhinoceros](https://www.flexicad.com/).

It is important to know the measures of the screw, in my case the screw was a M8
so i had to make my sketch suitable to the screw. On the sketch you can see the the measures.

![](../images/week03/week3_stand.JPG)

## Download link
[Download](../files/week3_cube.f3d)
