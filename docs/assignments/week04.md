# 4. 3D Printing

This week we had an overview about the 3D printing technology. We have to print a part with the 3D printer to attach two mechanical components.

## Design of my 3D part
![](../images/week04/week4_3dpart.png)
First of all I designed my part for this assignment in Fusion360

#### Step 1:
![](../images/week04/week4_sketch.png)
I make a triangle sketch.

#### Step 2:
![](../images/week04/week4_extrude1.png)
And extrude this 20mm.

#### Step 3:
![](../images/week04/week4_shell.png)
After the extrusion I make a shell inside of the model.

#### Step 4:
![](../images/week04/week4_extrude2.png)
For the screws I made extrusion inside of the model. And exported this model as an obj-file. And opened this in Cura.

![](../images/week04/week4_3d_cura.png)

I used the Ultimaker 2 Extended+ with 0,8mm PLA.

## Printing Settings for Cura
![](../images/week04/week4_cura_settings.png)


## How it looks
![](../images/week04/week4_photo.JPG)
